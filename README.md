# Customized kube-prometheus for Kubernetes Monitoring with Prometheus workshop

Ready-to-use project for the workshop exercises.

You can also fork the project and continue building your own customizations. Inspect the Git history to learn more about applied changes. 

## Resources

- [Documentation](https://github.com/prometheus-operator/kube-prometheus#customizing-kube-prometheus)
- [Workshop](https://gitlab.com/everyonecancontribute/workshops/kubernetes-monitoring-prometheus/prom-mon-k8s-2022)

## Development

- [jsonnet examples](https://github.com/prometheus-operator/kube-prometheus/tree/main/examples)

### Instructions

Create a new file called `ws-prom.jsonnet` and continue modify and building.

```
cp example.jsonnet ws-prom.jsonnet
```

Update the jsonnet dependencies for kube-prometheus:

```shell
$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci jb update
```

Compile the jsonnet files.

```shell
$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci ./build.sh ws-prom.jsonnet
```

Apply the manifests into the Kubernetes cluster.

```shell
$ kubectl apply --server-side -f manifests/setup
$ kubectl apply -f manifests/
```

### Custom CRDs

- Blackbox probes: https://doc.crds.dev/github.com/prometheus-operator/kube-prometheus/monitoring.coreos.com/Probe/v1@v0.8.0 

### Create your own

The following steps were applied to initially create this project.

```shell
$ mkdir my-kube-prometheus; cd my-kube-prometheus-2022

$ wget https://raw.githubusercontent.com/prometheus-operator/kube-prometheus/main/example.jsonnet -O example.jsonnet
$ wget https://raw.githubusercontent.com/prometheus-operator/kube-prometheus/main/build.sh -O build.sh && chmod +x build.sh

$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci jb init
$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci jb install github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus@main

$ vim example.jsonnet

$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci jb update
$ docker run --rm -v $(pwd):$(pwd) --workdir $(pwd) quay.io/coreos/jsonnet-ci ./build.sh example.jsonnet
```
